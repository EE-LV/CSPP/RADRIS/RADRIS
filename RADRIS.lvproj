﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str">CSPP_WS7AppInstalled,True;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "RADRIS.lvproj" is used to develop the RADRIS Control System Project and is based on LVOOP, NI Actor Framework and CS++.

Related documents and information
=================================
- README.md
- EUPL v.1.1 - Lizenz.pdf, EUPL v.1.1 - Lizenz.rtf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/RADRIS/RADRIS
- Documentation:
  - Refer to Documantation Folder and 
  - onenote:https://spf.gsi.de/sites/NIUser/Freigegebene%20Dokumente/NIUser2019/RADRIS/
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

Author: H.Brand@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="varPersistentID:{0000AD08-84A6-44BA-9812-426A3CEBDAA9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Reset</Property>
	<Property Name="varPersistentID:{0024B96A-736D-46E7-92D6-21FBAA051888}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingTime</Property>
	<Property Name="varPersistentID:{008ADF72-8F40-4285-B135-9D032CA05BF2}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{0091EB72-8D0F-483C-8795-451570E6C8C0}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{01A3AE54-2157-4D3A-825A-2B358E2B7C30}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_ErrorCode</Property>
	<Property Name="varPersistentID:{02DFBB54-E72D-4736-9638-CD7B8D630DC9}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{030643C8-DD68-463C-A4C2-DB70B153F854}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{03FF7381-3990-447F-82C4-63BDC0127088}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_DriverRevision</Property>
	<Property Name="varPersistentID:{046E2F5C-3741-4F6A-84C1-5468BD3D9A5B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_1</Property>
	<Property Name="varPersistentID:{05C05FCF-EF26-4799-AA81-384A26809F0C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_0</Property>
	<Property Name="varPersistentID:{06DD6B1F-CB3F-4360-A914-4D8A9C2D5A47}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Mode</Property>
	<Property Name="varPersistentID:{06F72C0B-C562-4386-B252-6D194F0397D2}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_7</Property>
	<Property Name="varPersistentID:{08B9067A-3448-49C0-96A1-7CD2B7D7256D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_4</Property>
	<Property Name="varPersistentID:{08DB6B08-9442-4A6D-AD2D-F8A197CA79F8}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{0A30C4F2-FA4F-47F0-8C12-97BEEFE776D7}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_4</Property>
	<Property Name="varPersistentID:{0ACC969B-7D38-4B44-B252-C0D8D0A43A93}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_2</Property>
	<Property Name="varPersistentID:{1077A716-AFA2-43AD-B658-4B1F53EB1B8B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_5</Property>
	<Property Name="varPersistentID:{10940883-63E4-40EB-898D-7A751D79D132}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID-AutoSpeed</Property>
	<Property Name="varPersistentID:{11AF52D5-D54E-45A0-AA6F-B279131FEDC9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_1</Property>
	<Property Name="varPersistentID:{13155329-D876-4E18-AA58-728A0706359B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_1</Property>
	<Property Name="varPersistentID:{13495D1A-101A-4F59-951E-0A03E27925FC}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_4</Property>
	<Property Name="varPersistentID:{13D5910B-77D5-4436-A61D-0EB5DFFC986F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PiraniRangeExtentsion</Property>
	<Property Name="varPersistentID:{1431DC59-51D4-401A-80F2-FCF86CBF7244}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_2</Property>
	<Property Name="varPersistentID:{17323D21-61E2-40CA-BEC5-B65F5613782A}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_ErrorMessage</Property>
	<Property Name="varPersistentID:{17A8F9EC-C66E-4AEC-9E85-61D2621F0C3A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{18A56387-501E-4636-9647-A2C0AC08293D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_ErrorMessage</Property>
	<Property Name="varPersistentID:{1AF1F538-CE53-43F3-A2F4-7DC3D77EC795}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-Auto-Reset</Property>
	<Property Name="varPersistentID:{1B62C37B-0D66-4B3A-B43A-C61120DA1A9D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingTime</Property>
	<Property Name="varPersistentID:{1B74F8A7-D563-449A-B8E6-0C01EF8E6DBA}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{1BAB6093-1BBC-45AB-8869-62674D9465C8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_1</Property>
	<Property Name="varPersistentID:{1C2E7677-B34A-474B-8F0A-EE0B7E0F52FD}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{1C47F613-AA81-4162-BA8F-863630484170}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{1D34EB9A-B9CB-4199-9C39-113E2B330F56}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_3</Property>
	<Property Name="varPersistentID:{1E038AE4-21C8-4222-A1BB-7A8C592EF69A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_1</Property>
	<Property Name="varPersistentID:{1E05B8B0-9FFA-4F87-87B4-C1DC09A479F3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingCounter</Property>
	<Property Name="varPersistentID:{1E51A49D-3B70-4D15-801D-90E4F0A9819B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_3</Property>
	<Property Name="varPersistentID:{1E603155-3C92-45AD-A0C4-23192381E57A}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{1FCABEF4-A313-47CB-BE70-87D3E8FCAAE7}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_5</Property>
	<Property Name="varPersistentID:{2263FB4A-8172-41C6-BCEF-1E75BEDF57D1}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_2</Property>
	<Property Name="varPersistentID:{22ADCCD9-CE8A-4DDD-B55F-DAE590A77050}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_0</Property>
	<Property Name="varPersistentID:{22F1E1C2-EDF8-4131-999E-0E04037C722B}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingInterval</Property>
	<Property Name="varPersistentID:{2392954D-2898-41C5-A521-58B5E49FF88B}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{24D616B3-CF9C-4E55-9AF8-1903D2C5589D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_1</Property>
	<Property Name="varPersistentID:{2505B735-409B-4FC2-BA47-07226067BC98}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_3</Property>
	<Property Name="varPersistentID:{254A2338-9444-4F31-819F-D2A07F206EA4}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_4</Property>
	<Property Name="varPersistentID:{254D2515-C7FA-4374-86F9-780933DFAA97}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{2643C72D-59A6-44F3-8D29-9AE2AA020EC1}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Valve-Temperature</Property>
	<Property Name="varPersistentID:{26915AAF-9F42-4BB3-AD55-2CDBCFC85615}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{269ED8C9-BA8F-4602-A18A-8D86D41EBC2B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{2786FECF-EECB-41BF-B27F-23C1D9414CEA}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_ErrorCode</Property>
	<Property Name="varPersistentID:{27A622C8-52CC-423B-A8DB-740F1789F8A5}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_3</Property>
	<Property Name="varPersistentID:{2A4F2EC3-658E-4FCC-8762-29DCFFEB226B}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{2AE11BBD-E775-4AC7-B283-2682E2098B73}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{2B8C8B6A-92C5-4ECA-B51C-3C4AB1BB3FF9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_0</Property>
	<Property Name="varPersistentID:{2D62AEAF-FCC5-4D12-8593-69EFDB02F184}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_2</Property>
	<Property Name="varPersistentID:{2FD4A255-ED87-4C63-84CA-9D157CE24156}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_4</Property>
	<Property Name="varPersistentID:{3050F1AD-777E-4BFA-BB01-3974B2DCA0F6}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_Reset</Property>
	<Property Name="varPersistentID:{3185D133-89B5-405A-B7EA-FC956819B028}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_TorrLock</Property>
	<Property Name="varPersistentID:{31D7B2CF-B4ED-44E9-9136-2E86BB2ED011}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Error</Property>
	<Property Name="varPersistentID:{33EC1DBD-5B35-4DA2-A824-8FB8771AB78F}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_LDAQ_WDAlarm</Property>
	<Property Name="varPersistentID:{355EAB33-7297-434E-82C9-0FAF51CFC5C6}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{3627F438-F149-473E-ADA6-B62131CEA174}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_D</Property>
	<Property Name="varPersistentID:{364472E0-A6D4-4B31-8131-7B15BE35CCBA}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366-Proxy_Activate</Property>
	<Property Name="varPersistentID:{364A5784-DD7F-4FD3-947C-A8C682244ED3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{36F252AA-45CC-4511-87E5-5A1C536CA11E}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_SelftestResultCode</Property>
	<Property Name="varPersistentID:{373101C7-708C-44D7-ABFC-9D3F329FC590}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_4</Property>
	<Property Name="varPersistentID:{3769144B-E35D-46A3-B589-B4B094F4867E}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_UnderrangeControl</Property>
	<Property Name="varPersistentID:{398F8797-2EB4-4D02-B8D1-66144AF18F75}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingInterval</Property>
	<Property Name="varPersistentID:{39CF96A1-E787-43FF-96A4-9EF46BB8AA8E}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_TPG366_WDAlarm</Property>
	<Property Name="varPersistentID:{3C5E664F-73B2-4D93-86F4-DC0C7D562043}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3CBB1256-2766-4489-B27E-798EBBCEC590}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_2</Property>
	<Property Name="varPersistentID:{3D0412A2-CE84-4382-862E-6740E141DB3D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_4</Property>
	<Property Name="varPersistentID:{3D160649-BC55-4D21-BC04-DE2D7EDEE68A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{3D8B2980-E228-49B6-A3BF-B4FFFC53FB00}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactors</Property>
	<Property Name="varPersistentID:{44C2F618-47C8-43D1-9FAC-975032BA9172}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_DriverRevision</Property>
	<Property Name="varPersistentID:{4635FEE1-8FBB-4CE2-A05F-13C2E1978DFD}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_1</Property>
	<Property Name="varPersistentID:{46BD4B7E-CDF5-4A10-9D4F-4AB52E27A042}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_1</Property>
	<Property Name="varPersistentID:{48DB4D62-79AE-4D78-A08E-B9F64394559A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PressureStatus</Property>
	<Property Name="varPersistentID:{4B8D28BE-9AB2-497C-8B02-6D6E16A0EF10}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_4</Property>
	<Property Name="varPersistentID:{4B9317DC-EE8C-461C-B8F7-E6F6BE227BED}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{4E497048-B2AA-41AA-A741-DCCCB2DB5B72}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQProxy_Activate</Property>
	<Property Name="varPersistentID:{4EA9913C-6D42-4099-886F-10FF622E59DF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingIterations</Property>
	<Property Name="varPersistentID:{520E91B1-92F3-4C51-8A42-DC81EC5B93CE}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{5270E4EA-B361-4E14-A2A6-6D6AF10774EF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID-I</Property>
	<Property Name="varPersistentID:{530919F7-810F-4A93-ACD5-DD11BBFF9E40}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_2</Property>
	<Property Name="varPersistentID:{53A72787-12E8-469E-AAF9-1F40550C3468}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas</Property>
	<Property Name="varPersistentID:{5429084D-1828-43E6-8C51-792458CBFACE}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Keylock</Property>
	<Property Name="varPersistentID:{5611431E-4BD3-4B36-A3D8-A9536DB1E979}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Error</Property>
	<Property Name="varPersistentID:{569A5BC9-6501-4C3D-930F-EEA4C227FA27}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_ResourceName</Property>
	<Property Name="varPersistentID:{56BB1CC0-B656-4409-8FCB-9473D28533FB}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTimes</Property>
	<Property Name="varPersistentID:{57682970-F919-4258-8377-E90640A97F22}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{59355711-24C8-445F-BBB2-3000C83A5A5F}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{5A19584B-229F-4A56-AA52-7CF58D989E72}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_4</Property>
	<Property Name="varPersistentID:{5A326B32-6580-4595-A11C-3803287749D9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID</Property>
	<Property Name="varPersistentID:{5BEFD593-3759-4F6B-8B86-FA64F8C081BF}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5C50A077-2FB0-40E9-9217-98A7412C2CA4}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_ErrorStatus</Property>
	<Property Name="varPersistentID:{5E77C13A-D6FF-442D-907C-5D59052E1B49}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Valve-Position</Property>
	<Property Name="varPersistentID:{5F4F41CE-308D-449F-9D27-932FE4A4678B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_2</Property>
	<Property Name="varPersistentID:{60AE8956-F8DF-4AE2-BF0D-D0394BC5EAA2}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressures</Property>
	<Property Name="varPersistentID:{620A3D21-098D-4022-8650-4BC6B6B52561}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_3</Property>
	<Property Name="varPersistentID:{623F0593-0805-4DAB-B58B-AE8924246F97}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits</Property>
	<Property Name="varPersistentID:{626C5718-7242-49F8-8FC7-1BF600E89FDD}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_1</Property>
	<Property Name="varPersistentID:{6294675A-B8F6-4B64-9AE7-E1F639F5BB79}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_4</Property>
	<Property Name="varPersistentID:{64D00E69-AFF9-44B1-9F50-03BC99039A71}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_ErrorStatus</Property>
	<Property Name="varPersistentID:{65072B2F-DEED-4158-AA94-9F1BDA8505EA}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_RVC300_WDAlarm</Property>
	<Property Name="varPersistentID:{65FDCD7B-5248-4C40-997A-B775579D22D2}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{669ADF69-419A-4015-8FE0-9DDAFD0CA142}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingMode</Property>
	<Property Name="varPersistentID:{680E1E36-B231-4419-B7F5-1E8823D2619E}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{68929B61-DB00-4D9C-8D89-52374D9F9BE3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-WatchdogControl</Property>
	<Property Name="varPersistentID:{6AED43B1-757D-436E-AC1E-7E584F82866A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_5</Property>
	<Property Name="varPersistentID:{6B5F140C-5C45-41CC-92F2-3ADDDDB722E7}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_FirmwareRevision</Property>
	<Property Name="varPersistentID:{6BC0910A-ADB1-4FDD-A2DB-6BC187B9E205}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Valve</Property>
	<Property Name="varPersistentID:{6C869C8A-AA6E-4215-B90C-F5D73BB7FFC4}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_1</Property>
	<Property Name="varPersistentID:{6CF73997-45AF-4FED-B8E2-FBB2D16F2BE9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SelfTest</Property>
	<Property Name="varPersistentID:{70400048-E2F7-4D7F-AB5F-4B903A4BDDE0}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{70B1AA27-A8B2-4FB9-A395-33D4EAAF3D62}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_3</Property>
	<Property Name="varPersistentID:{721AD97B-0BD1-46E7-9EBC-5B001E163770}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{721E081C-1BB8-41A3-A731-31CEF4C07AF8}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_2</Property>
	<Property Name="varPersistentID:{72C3D279-F15F-491E-B85B-5D407764EE9F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300Proxy_Activate</Property>
	<Property Name="varPersistentID:{732A308C-8823-4FD1-9EC9-2B977B0C03DD}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{738213D0-0FA9-49D1-ACDB-3AFF47D056A8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Auto-Reset</Property>
	<Property Name="varPersistentID:{747C3347-83A7-4E63-80EC-B1FB5F0041D8}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{756BAD04-2C74-4421-9C38-06D241BE0DE5}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_5</Property>
	<Property Name="varPersistentID:{770D2E47-42EC-472A-86AE-2DD40B23C671}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FirmwareRevision</Property>
	<Property Name="varPersistentID:{78A29A79-4C36-41EF-AB63-5DD4ABE9410B}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{7A4AF1A0-0133-43B1-9B8B-3B862BA37175}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SelftestResultCode</Property>
	<Property Name="varPersistentID:{7B02DF4B-B553-42DA-A1BF-3550000CB914}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingTime</Property>
	<Property Name="varPersistentID:{7B5EE98D-9D0B-4DA9-BE98-1CD84DEA074A}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingMode</Property>
	<Property Name="varPersistentID:{7B7A02C3-82BB-4630-9901-C024E7CB0FBF}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{7ED33E14-5219-4004-8A77-C554C4B5229F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscales</Property>
	<Property Name="varPersistentID:{7FFC0480-81EB-4A15-9154-3FC24A3125E8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_4</Property>
	<Property Name="varPersistentID:{825CB1F2-D4EB-4AFE-B4A7-AA0FF747BAF9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_0</Property>
	<Property Name="varPersistentID:{82B837AF-6AB0-4326-A8CA-CED9143FEE85}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID-D</Property>
	<Property Name="varPersistentID:{838BC189-D39D-4F47-B7F5-93BCFB370617}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_2</Property>
	<Property Name="varPersistentID:{84F3C5C1-A645-45A5-A161-54352EF568B8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_2</Property>
	<Property Name="varPersistentID:{85F1F755-6F8A-45B7-BFA2-53EEBF5CC7A3}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{87E62CA1-B845-4159-B4C7-A0E3E11D5396}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_2</Property>
	<Property Name="varPersistentID:{8852F0A0-5DD8-4131-B0AA-6DF678A312B8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_ErrorCode</Property>
	<Property Name="varPersistentID:{88AFCE46-921A-46BD-A590-D63094F75262}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrections</Property>
	<Property Name="varPersistentID:{89A33A9A-94A2-4354-AE9C-D5D5B57F758B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_4</Property>
	<Property Name="varPersistentID:{89CC59E0-437D-476E-948B-55FB16FC1D80}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_5</Property>
	<Property Name="varPersistentID:{8A520F4D-FF54-479D-A697-FBB8A15327F3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_1</Property>
	<Property Name="varPersistentID:{8A56342B-7968-4FCC-8F3B-34102B315373}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{8ABF97AF-712D-4768-A2D5-8BDDF8298BCA}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_4</Property>
	<Property Name="varPersistentID:{8AF488FF-1A48-4364-93FE-0F2FE1C8F1E5}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{8EDA9573-C16F-4E3F-8DCD-D4E7E1EBE66C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingCounter</Property>
	<Property Name="varPersistentID:{911F5C44-0462-4795-850C-F6B003088FEB}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID-P</Property>
	<Property Name="varPersistentID:{91F73D58-A3AD-4B83-95B3-CF84AE7428EF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Temperature</Property>
	<Property Name="varPersistentID:{92A9B19F-CEBE-495B-BAAD-EE936BFC47A5}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_3</Property>
	<Property Name="varPersistentID:{93DE04D7-2EDD-421D-9976-13B7C53D77D9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_3</Property>
	<Property Name="varPersistentID:{94002D06-14EA-4559-A349-8CA4DC2657C1}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingDeltaT</Property>
	<Property Name="varPersistentID:{9428819B-2448-41A5-9220-8CD7CAA000A5}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_3</Property>
	<Property Name="varPersistentID:{96799DF1-A9BE-4BBD-840E-2AA72C38D816}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_D</Property>
	<Property Name="varPersistentID:{97B79147-0778-4E47-AC99-E0FB73E670F2}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_1</Property>
	<Property Name="varPersistentID:{983C0475-CA9E-47EF-986F-272907661024}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{99411EBF-D39B-4FE9-9D1E-04D40360CF66}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_1</Property>
	<Property Name="varPersistentID:{9A082721-F8DE-48D7-A0CC-E21BB8CC2529}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Flow-Status</Property>
	<Property Name="varPersistentID:{9AA52EFA-EDE4-44B3-8F29-3E567C842CE5}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{9BB8D78E-C4B1-4752-A325-C25A6FAD1548}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{9C8FE1E0-E181-4E38-8FA6-A7DA2DCB5B46}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{9D79D759-8736-4851-ADD8-A26788985A58}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_3</Property>
	<Property Name="varPersistentID:{9E5649D2-181F-4D8D-A705-E187BC4B05C0}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_2</Property>
	<Property Name="varPersistentID:{9E83348E-D454-4743-B5E3-A37151187993}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{9E9603D9-0170-4829-8A75-E7556C9B0DBA}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PID-AutoSpeed</Property>
	<Property Name="varPersistentID:{A0A19C17-42FF-4C88-9CA9-B3E5F285635F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{A0E54E43-6621-48FC-B072-9C475EDF07AD}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{A1EE1416-0FC7-42CD-9813-CF749DB968B0}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-PiraniRangeExtentsion</Property>
	<Property Name="varPersistentID:{A3833617-9982-4DA0-9466-1D2AAF3807B7}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_0</Property>
	<Property Name="varPersistentID:{A3962405-D9CA-40FC-A302-F7A6EDE942BE}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PID-D</Property>
	<Property Name="varPersistentID:{A5353FB4-5547-4B23-BC72-15916B7E4320}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Valve-Status</Property>
	<Property Name="varPersistentID:{A5566FFC-5781-466B-84F0-1E29FD0C5AC1}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_ErrorMessage</Property>
	<Property Name="varPersistentID:{A55D148F-557E-4997-899D-BED2A6E7970A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{A5ACAF74-22C2-4C60-9306-637171EBBA21}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{A61E8CD5-7BCA-450C-8BBB-E054F18972CF}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{A6C70853-2AA9-4EBB-A53C-A3F4D97052EB}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_1</Property>
	<Property Name="varPersistentID:{A836A7CE-A3FA-4FF7-9953-60D74B38A2C4}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI</Property>
	<Property Name="varPersistentID:{A8DD0F8B-695C-4A56-AF62-7845C56BA5F3}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{AC99F414-D553-4DF6-A46D-5F0B867A213D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_5</Property>
	<Property Name="varPersistentID:{ADB47E9B-510E-47AD-8C90-A0C5C8854B31}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Reset</Property>
	<Property Name="varPersistentID:{AF998F11-5262-4C21-8DB3-2034CE44B1DB}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_2</Property>
	<Property Name="varPersistentID:{B0093FE1-0FF7-40DB-8586-E8C9AD86C026}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_5</Property>
	<Property Name="varPersistentID:{B127CCCE-A159-4C28-A4DB-A8C423F3C964}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_3</Property>
	<Property Name="varPersistentID:{B166C6A1-778D-460A-B0F6-F3894169261D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_3</Property>
	<Property Name="varPersistentID:{B2195B1C-E818-4829-8FAC-B4CE25D139BF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingStartStop</Property>
	<Property Name="varPersistentID:{B442317F-8150-485F-A186-A67A5CD6EC30}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasTypes</Property>
	<Property Name="varPersistentID:{B4C53D8C-3F54-4EDC-BDE7-673E82902158}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_0</Property>
	<Property Name="varPersistentID:{B6464CDD-C246-4E75-B46C-FC869B351254}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{B734EB68-8BD8-4A30-857A-C82816A0E693}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_5</Property>
	<Property Name="varPersistentID:{B864E2B5-F2AB-490F-854B-04DC03308678}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Flow-Setpoint</Property>
	<Property Name="varPersistentID:{B953E074-2386-4628-848E-6BB7D4D14454}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_2</Property>
	<Property Name="varPersistentID:{BA2BE91A-9C72-4A3E-9FD6-1E6AA98BFC8C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Fullscale_0</Property>
	<Property Name="varPersistentID:{BA57706D-7114-4995-A83E-1E2338A3FE34}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_ResourceName</Property>
	<Property Name="varPersistentID:{BB1300C8-5A54-4130-9A08-B2193F24993D}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BC3AF6B0-B75E-4EE0-8F88-92A95CED3381}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_FirmwareRevision</Property>
	<Property Name="varPersistentID:{BE1F3574-8737-464E-AE5D-B750274C6D9B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingInterval</Property>
	<Property Name="varPersistentID:{BE385D1E-1F6B-4D5C-86B3-DDEFEB864D65}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_5</Property>
	<Property Name="varPersistentID:{C04DF69F-6EF4-4526-99B9-868FFA3DB06F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOns</Property>
	<Property Name="varPersistentID:{C113AEF4-560E-4E03-85D6-870842DE6DDE}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_SelfTest</Property>
	<Property Name="varPersistentID:{C2F55749-140D-4B42-B382-13D0B78282C5}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_D</Property>
	<Property Name="varPersistentID:{C40583C6-C30A-42B3-B406-6D1BA6CDEE72}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_5</Property>
	<Property Name="varPersistentID:{C5EF8345-25B3-4E30-8BC0-C07294790973}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_4</Property>
	<Property Name="varPersistentID:{C6D1A2DC-06CC-4F2E-8FF6-27464B06651C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_2</Property>
	<Property Name="varPersistentID:{C7DB6054-CF87-4B98-A54D-F107B88512C4}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_DriverRevision</Property>
	<Property Name="varPersistentID:{C8AD7087-33DE-4C37-8A48-8977912FA81A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{C8C67597-8DCA-44C0-B894-08B756184EE7}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_0</Property>
	<Property Name="varPersistentID:{CA77DD7F-FCF6-4F9D-8411-64E82E00179F}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{CC17F213-1F62-4AC5-921A-CCB6D539C52B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS</Property>
	<Property Name="varPersistentID:{CCD9AF16-AED4-4BE2-A1FD-9815A5D103EC}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Pressure</Property>
	<Property Name="varPersistentID:{CDC6B78F-EA85-4486-9871-2B23CBF29C74}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{CDCA738B-8C34-43C3-B5CD-3AFE44A4EA0B}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{CE05AF0E-8082-43A4-9EE5-55D8A4963A33}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingIterations</Property>
	<Property Name="varPersistentID:{CEA3F0A8-78E3-420D-AC87-EF69265712EF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_PollingDeltaT</Property>
	<Property Name="varPersistentID:{D10A12F2-56F9-410D-A2DF-D9B0D32E4AF9}" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{D1DF2954-96D5-4ED1-9D15-701EC204C9F2}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_4</Property>
	<Property Name="varPersistentID:{D2988AB6-25C2-4F4D-A762-5C9FB9E3B4BE}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_SelfTest</Property>
	<Property Name="varPersistentID:{D2BF18AF-389C-442B-8395-EFA3FFBAB3C2}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Degas_5</Property>
	<Property Name="varPersistentID:{D43A8D65-7887-4C02-B9EB-0E51F0DFB0F8}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-FilterTime_4</Property>
	<Property Name="varPersistentID:{D4B62D6B-642F-4B6C-A4E7-215137A2A926}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-Mode</Property>
	<Property Name="varPersistentID:{D53F2CFA-31D8-4AB6-882C-9189E6A7FD57}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_1</Property>
	<Property Name="varPersistentID:{D74B2935-DC02-4EBC-8329-0AF01BFAE873}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingCounter</Property>
	<Property Name="varPersistentID:{D8E2158B-6856-455F-BB95-1CE0B3B3C68C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_0</Property>
	<Property Name="varPersistentID:{D9D5FE58-2690-4227-8239-7A18F3E41785}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{D9E4FD3C-5117-4BD4-B9B1-F644DA85FBBB}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_3</Property>
	<Property Name="varPersistentID:{DAFFC067-C5A8-474F-9DD3-AB14D6611897}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_0</Property>
	<Property Name="varPersistentID:{DBD961FF-18DF-49BE-BBFC-FAD40961ED4E}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-GasType_3</Property>
	<Property Name="varPersistentID:{DD1E9CF0-846C-48B8-B886-C31CCDF51A2C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_GasType_5</Property>
	<Property Name="varPersistentID:{DD4D62C1-0A10-41D2-88C1-51CBA168E219}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_2</Property>
	<Property Name="varPersistentID:{DE9AEF45-4891-444C-9B3D-753770A937BF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-Pressure-SetPoint</Property>
	<Property Name="varPersistentID:{DF83ED01-DB8F-4445-966F-D1E04780C303}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PID</Property>
	<Property Name="varPersistentID:{DFA24382-E2BC-47AC-99A3-036F878E6690}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{E1A88B9B-4F7E-4555-A4E5-085078E59356}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_ErrorStatus</Property>
	<Property Name="varPersistentID:{E47903CE-DD93-44C6-A890-A887F64E7475}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_6</Property>
	<Property Name="varPersistentID:{E520C299-B5F8-4F2E-9BD5-5990EADFDB45}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPtGraphProxy_Activate</Property>
	<Property Name="varPersistentID:{E5CAD5DF-FDD3-4DCF-83C8-224F4300E99E}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_4</Property>
	<Property Name="varPersistentID:{E80898EC-EFBC-4673-8CCD-87C7D79B619F}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_PollingIterations</Property>
	<Property Name="varPersistentID:{EA6B3567-1CB2-4594-94D5-294E2DC6C9D0}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PID-I</Property>
	<Property Name="varPersistentID:{EA818CEB-6549-4BD8-BFCF-C5150B76BEFE}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{EB6F9A4F-A11D-4DFF-97E4-E7069B5BD37E}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-Flow-SetPoint</Property>
	<Property Name="varPersistentID:{ED51D795-CD14-403F-9D14-BB99E1397CE3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_3</Property>
	<Property Name="varPersistentID:{EDABCBB0-4CFD-4068-919F-78E53354805B}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_OffsetCorrection_0</Property>
	<Property Name="varPersistentID:{EEBDB499-DAD1-473F-B344-4AD7F33192F5}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PollingMode</Property>
	<Property Name="varPersistentID:{EF4E5DD6-8557-4BC0-AA4E-9CD8B10237DD}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_ParameterLock</Property>
	<Property Name="varPersistentID:{F01C8D24-7580-4AA7-8E01-9D3AAAB21D31}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-CalibrationFactor_1</Property>
	<Property Name="varPersistentID:{F138076E-478A-48AD-81E5-3B6CBF218E9C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Pressure_0</Property>
	<Property Name="varPersistentID:{F2BA8998-F201-4420-ACA5-813803307FBE}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_SelftestResultCode</Property>
	<Property Name="varPersistentID:{F34BE25D-A3E9-4ADC-AF5A-D64701769623}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status</Property>
	<Property Name="varPersistentID:{F3DECAB1-5757-42D3-A114-CA693B58CBC3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Degas_5</Property>
	<Property Name="varPersistentID:{F43D3982-CD41-4ADA-A701-3CCE29E92770}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Set-PID-P</Property>
	<Property Name="varPersistentID:{F499C0EF-400F-48B7-8342-D96C9B6A4596}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_0</Property>
	<Property Name="varPersistentID:{F4CCA323-8327-4DA1-960B-7870B1FA7A88}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SPS_0</Property>
	<Property Name="varPersistentID:{F69959DF-F507-40F9-AB38-36ED3852B8C3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{F736D35F-8D8A-4094-8094-91959F9AADA9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_FilterTime_0</Property>
	<Property Name="varPersistentID:{F73F3953-2F70-4FCE-A478-E953C44C247C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_CalibrationFactor_0</Property>
	<Property Name="varPersistentID:{F7E6059B-1DA0-46B5-86DB-748318C0A23A}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-SensorOffOn_5</Property>
	<Property Name="varPersistentID:{F88B0269-BE7C-4177-86FC-A29765EDDDA2}" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{F90B178A-F82A-45BD-AF92-8F083163B827}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-TorrLock</Property>
	<Property Name="varPersistentID:{F94502C8-7ACB-4239-8E5D-F35DFBCE3D6C}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_ResourceName</Property>
	<Property Name="varPersistentID:{FA7C0623-603E-4A44-9271-6084C74AA1BC}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{FAE72AEB-8876-49AB-814A-7DFC97DAB346}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SensorOffOn_5</Property>
	<Property Name="varPersistentID:{FB0212E8-3E72-46C4-9178-548A9D96A0F0}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_AI_3</Property>
	<Property Name="varPersistentID:{FB2A5FF2-379D-41A8-B31C-B80A333DEEBF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Set-Fullscale_5</Property>
	<Property Name="varPersistentID:{FBAA4D4C-D92A-453F-9D51-5A035471FDDF}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_WatchdogControl</Property>
	<Property Name="varPersistentID:{FC0AC567-D523-4FBA-BA57-88F5C63E96D9}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_Pressure-Setpoint</Property>
	<Property Name="varPersistentID:{FD3EF639-17FC-4483-86EA-5D5D2FCB6E65}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{FE6E9691-D318-4921-985D-C3F44B5B8BC3}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/RVC300_PID-Type</Property>
	<Property Name="varPersistentID:{FEE89D2F-AFAF-4457-8ADE-048053A5FBE9}" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib/LPDAQ_Error</Property>
	<Property Name="varPersistentID:{FFD02DA2-44C9-423E-94CF-38471796BFD6}" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib/TPG366_Status_5</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Docs &amp; EUPL" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="PV ActiveLine.lvlib" Type="Library" URL="../instr.lib/PVActiveLine/PV ActiveLine.lvlib"/>
			<Item Name="RCV 300.lvlib" Type="Library" URL="../instr.lib/RVC300/RCV 300.lvlib"/>
		</Item>
		<Item Name="lib.lib" Type="Folder">
			<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
			<Item Name="GitControl.lvlib" Type="Library" URL="../Packages/gitversioninfo/GitControl.lvlib"/>
		</Item>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
		</Item>
		<Item Name="CSPP" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="BNT" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
				<Item Name="BNT_XtGraph.lvlib" Type="Library" URL="../Packages/BNT_XtGraph/BNT_XtGraph.lvlib"/>
			</Item>
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
				<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Core/README.md"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				<Item Name="README.md" Type="Document" URL="../Packages/CSPP_DSC/README.md"/>
			</Item>
			<Item Name="Laser" Type="Folder">
				<Item Name="CSPP_WS7.lvlib" Type="Library" URL="../Packages/CSPP_Laser/CSPP_WS7.lvlib"/>
			</Item>
			<Item Name="OM" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_ObjectManager/README.txt"/>
			</Item>
			<Item Name="PVConverter" Type="Folder">
				<Item Name="CSPP_PVScaler.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PVScaler.lvlib"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
				<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Utilities/README.md"/>
			</Item>
			<Item Name="Vacuum" Type="Folder">
				<Item Name="CSPP_RVC300-GUI.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_RVC300/CSPP_RVC300-GUI.lvlib"/>
				<Item Name="CSPP_RVC300.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_RVC300/CSPP_RVC300.lvlib"/>
				<Item Name="CSPP_TPG366-GUI.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG366/CSPP_TPG366-GUI.lvlib"/>
				<Item Name="CSPP_TPG366.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG366/CSPP_TPG366.lvlib"/>
				<Item Name="CSPP_Vacuum.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_Vacuum.lvlib"/>
			</Item>
		</Item>
		<Item Name="Devices" Type="Folder"/>
		<Item Name="GUIs" Type="Folder"/>
		<Item Name="RADRIS" Type="Folder">
			<Item Name="RADRIS_Content.vi" Type="VI" URL="../RADRIS_Content.vi"/>
			<Item Name="RADRIS_MainGUI.lvlib" Type="Library" URL="../Packages/RADRIS/RADRIS_MainGUI/RADRIS_MainGUI.lvlib"/>
		</Item>
		<Item Name="SV.lib" Type="Folder">
			<Item Name="RADRIS.lvlib" Type="Library" URL="../SV.lib/RADRIS.lvlib"/>
			<Item Name="LPDAQ.lvlib" Type="Library" URL="../SV.lib/LPDAQ.lvlib"/>
			<Item Name="CSPP_SystemMonitor_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor_SV.lvlib"/>
		</Item>
		<Item Name="RADRIS.ini" Type="Document" URL="../RADRIS.ini"/>
		<Item Name="RADRIS.ico" Type="Document" URL="../RADRIS.ico"/>
		<Item Name="RADRIS.vi" Type="VI" URL="../RADRIS.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="ALM_Make_Summary.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Make_Summary.vi"/>
				<Item Name="ALM_Test_Zero.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Test_Zero.vi"/>
				<Item Name="ALM_Test_For_Valid_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Test_For_Valid_Alarm.vi"/>
				<Item Name="ALM_Alarm_Status.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Alarm_Status.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get XY Value.vi"/>
				<Item Name="DTbl Get Digital Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Get Digital Value.vi"/>
				<Item Name="WDT Get XY Value DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value DBL.vi"/>
				<Item Name="WDT Get XY Value I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I64.vi"/>
				<Item Name="WDT Get XY Value I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I32.vi"/>
				<Item Name="WDT Get XY Value I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I16.vi"/>
				<Item Name="WDT Get XY Value EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value EXT.vi"/>
				<Item Name="WDT Get XY Value CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CDB.vi"/>
				<Item Name="WDT Get XY Value CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CXT.vi"/>
				<Item Name="DWDT Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get XY Value.vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Control Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="VISA Lock Async.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Lock Async.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
			<Item Name="wlmData.dll" Type="Document" URL="wlmData.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="RADRIS App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{CAA48AF5-FF02-45CB-A60A-4750CE7B557F}</Property>
				<Property Name="App_INI_GUID" Type="Str">{624B6B9E-C65B-4581-80EE-4F3C4C8B6582}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/RADRIS.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="App_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="App_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B02E8DD7-41E0-46E6-9861-3FF4621EAB4B}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">RADRIS App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/NI_AB_PROJECTNAME/RADRIS App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/CSPP/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_preActionVIID" Type="Ref">/My Computer/lib.lib/GitControl.lvlib/Pre-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FC2E7B98-D7A6-46C6-85F6-8B5CC1A47099}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/RADRIS/RADRIS App/RADRIS_RADRIS App_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Destination[0].destName" Type="Str">RADRIS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/RADRIS App/RADRIS.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/RADRIS App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/RADRIS.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{2108E0DD-3D64-4C68-AB57-6CC7DB7FDE8F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/RADRIS.ini</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/RADRIS.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/CSPP/Vacuum/CSPP_Vacuum.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/SV.lib/LPDAQ.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/SV.lib/RADRIS.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/SV.lib/CSPP_SystemMonitor_SV.lvlib</Property>
				<Property Name="Source[9].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">RADRIS App</Property>
				<Property Name="TgtF_internalName" Type="Str">RADRIS App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">RADRIS App</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EE1250D6-3223-4B89-81AF-193099907912}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">RADRIS.exe</Property>
			</Item>
			<Item Name="RADRIS Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">RADRIS</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{07EF8BCC-92B8-4037-97F0-DC870C762E84}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{6C2EA93A-BE4B-4929-BC67-ECE3DC942BFB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 19.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{E60B4861-89AB-4E60-96C2-93AB25CC9AE4}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2019</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{CEF5E531-69E2-461E-8628-0998E4DD0317}</Property>
				<Property Name="DistPart[10].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[10].productID" Type="Str">{D15211BD-579A-43B8-9A0D-B53A5978C850}</Property>
				<Property Name="DistPart[10].productName" Type="Str">NI-DAQmx Runtime with Configuration Support 19.0</Property>
				<Property Name="DistPart[10].upgradeCode" Type="Str">{9856368A-ED47-4944-87BE-8EF3472AE39B}</Property>
				<Property Name="DistPart[11].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[11].productID" Type="Str">{791D0C60-DFC5-407D-B1A8-9E831D6A8C2E}</Property>
				<Property Name="DistPart[11].productName" Type="Str">NI-VISA Remote Server 19.0</Property>
				<Property Name="DistPart[11].upgradeCode" Type="Str">{951E7F56-F1CD-403C-B138-9BEFC6CEB343}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C02E174E-A57A-4B5F-A762-8FCA4854370A}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Remote Execution Support 2019</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{1DB06C72-60F2-48DC-BAEA-935A3CFFFA3C}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{FA0DB08E-BC18-4194-9ADC-026B7C8D5CEA}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI Variable Engine 2019</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{160B311C-5360-4299-BD7C-1C61EB18036E}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-488.2 Runtime 19.0</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{C53F7F6E-1ABA-482F-9DC6-E0DDB6002F00}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-DAQmx Runtime 19.0</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{923C9CD5-A0D8-4147-9A8D-998780E30763}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{902E9855-1E7F-464C-AEA1-6906519A14FF}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-Serial Runtime 19.0</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[7].productID" Type="Str">{EACEADFB-F69A-4FA2-8AF8-7A4A57C48A2D}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI-VISA Runtime 19.0</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[8].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[8].productID" Type="Str">{4D225A8F-9F0D-44C6-AD8C-6992B7A7F182}</Property>
				<Property Name="DistPart[8].productName" Type="Str">NI LabVIEW Runtime 2019 f1</Property>
				<Property Name="DistPart[8].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[8].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[8].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[1].productName" Type="Str">NI Deployment Framework 2019</Property>
				<Property Name="DistPart[8].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[8].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[10].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[8].SoftDep[10].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[8].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[11].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[8].SoftDep[11].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[8].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[2].productName" Type="Str">NI Error Reporting 2019</Property>
				<Property Name="DistPart[8].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[8].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2019</Property>
				<Property Name="DistPart[8].SoftDep[3].upgradeCode" Type="Str">{8386B074-C90C-43A8-99F2-203BAAB4111C}</Property>
				<Property Name="DistPart[8].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[4].productName" Type="Str">NI LabVIEW Runtime 2019 Non-English Support.</Property>
				<Property Name="DistPart[8].SoftDep[4].upgradeCode" Type="Str">{446D49A5-F830-4ADF-8C78-F03284D6882D}</Property>
				<Property Name="DistPart[8].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[5].productName" Type="Str">NI Logos 19.0</Property>
				<Property Name="DistPart[8].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[8].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2019</Property>
				<Property Name="DistPart[8].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[8].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[7].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[8].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[8].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[8].SoftDep[8].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[8].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[8].SoftDep[9].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[8].SoftDep[9].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[8].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[8].upgradeCode" Type="Str">{7D6295E5-8FB8-4BCE-B1CD-B5B396FA1D3F}</Property>
				<Property Name="DistPart[9].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[9].productID" Type="Str">{EBCDF6CD-76D1-48AF-A6D5-6721CDDA3C17}</Property>
				<Property Name="DistPart[9].productName" Type="Str">NI TDM Excel Add-In 2019</Property>
				<Property Name="DistPart[9].upgradeCode" Type="Str">{6D2EBDAF-6CCD-44F3-B767-4DF9E0F2037B}</Property>
				<Property Name="DistPartCount" Type="Int">12</Property>
				<Property Name="INST_author" Type="Str">GSI GmbH (DE)</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/D/builds/RADRIS/RADRIS Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">RADRIS Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{07EF8BCC-92B8-4037-97F0-DC870C762E84}</Property>
				<Property Name="INST_installerName" Type="Str">setup.exe</Property>
				<Property Name="INST_productName" Type="Str">RADRIS</Property>
				<Property Name="INST_productVersion" Type="Str">0.0.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008007</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123</Property>
				<Property Name="MSI_arpURL" Type="Str">https://www.gsi.de</Property>
				<Property Name="MSI_distID" Type="Str">{EEFAB0AC-4043-4363-A2D5-B95D900D8D32}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{299DB6EA-39BA-45E9-A068-AB01EE4D22D6}</Property>
				<Property Name="MSI_windowMessage" Type="Str">Thanks for installing the RADRIS Control System
Don't forget to modify RADRIS.ini to your needs and make it read-only.
You can use the DSCManager to dlopy preconfigured SV.lvlibs available in the support directory 'data'. Refer also to data/Readme.md.
You need to install the NI LabVIEW Data Logging &amp; Supervisory Control Module Runtime.</Property>
				<Property Name="MSI_windowTitle" Type="Str">Installer for the RADRIS Control System</Property>
				<Property Name="MSI_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="MSI_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="MSI_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{07EF8BCC-92B8-4037-97F0-DC870C762E84}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{07EF8BCC-92B8-4037-97F0-DC870C762E84}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">RADRIS.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">RADRIS</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">RADRIS</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{EE1250D6-3223-4B89-81AF-193099907912}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{07EF8BCC-92B8-4037-97F0-DC870C762E84}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">RADRIS.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{624B6B9E-C65B-4581-80EE-4F3C4C8B6582}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">RADRIS App</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/RADRIS App</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
