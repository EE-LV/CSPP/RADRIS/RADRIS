Readme
======
This LabVIEW project "RADRIS.lvproj" is used to develop the RADRIS Control System project and is based on LVOOP, NI Actor Framework and CS++.

LabVIEW 2018 is currently used development.

Related documents and information
=================================
- README.md
- EUPL v.1.1 - Lizenz.pdf, EUPL v.1.1 - Lizenz.rtf
- Contact: H.Brand@gsi.de
- Download, bug reports... : [https://git.gsi.de/EE-LV/CSPP/RADRIS/RADRIS](https://git.gsi.de/EE-LV/CSPP/RADRIS/RADRIS)
- Documentation:
  - Refer to Documentation folder
  - OneNote: [onenote:https://spf.gsi.de/sites/NIUser/Freigegebene%20Dokumente/NIUser2019/RADRIS/](onenote:https://spf.gsi.de/sites/NIUser/Freigegebene%20Dokumente/NIUser2019/RADRIS/)
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

GIT Submodules
=================================
Following git submodules are used in this project.

- Packages/CSPP_Core: containing the CS++ core libraries; [https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git)
- Packages/CSPP_ObjectManager; [https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git)
- Packages/CSPP_DSC: containing Alarm- & Trend-Viewer; [https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git)
- Packages/CSPP_Utilities; [https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git)

External Dependencies
=================================

Optional:
- Syslog; Refer to [http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116](http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116)
- https://git.gsi.de/p.chhetri/DyeLaser_Project.git](https://git.gsi.de/p.chhetri/DyeLaser_Project.git) contains the originial sources of the rudimentary laser control system.


Getting started:
=================================
- Create a project specific copy of "RADRIS.lvproj"
- You need to create your project specific ini-file, like "RADRIS.ini"
  - Sample ini-file should be available for all classes, either in the LV-Project or on disk in the corresponding class or package folder.
- You need to create and deploy your project specific shared Variable libraries.
  - Sample shared Variable libraries should be available for all concerned classes on disk in the corresponding class or package folder.
- Run your project specific _My Computer/RADRIS.vi_.
- Build application
  - The corresponding build specification _RADRIS App_ uses
    - _RADRIS.vi_ as startup-VI. It calls
    - _RADRIS\RADRIS___Content.vi_; Including your project specific Content-VIs 
  - Duplicate the build specification and adapt it to your needs.


Author: H.Brand@gsi.de

Copyright 2018  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: [http://www.osor.eu/eupl[(http://www.osor.eu/eupl)

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.