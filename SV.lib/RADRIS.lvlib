﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str"></Property>
	<Property Name="Alarm Database Name" Type="Str"></Property>
	<Property Name="Alarm Database Path" Type="Str"></Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str"></Property>
	<Property Name="Database Name" Type="Str"></Property>
	<Property Name="Database Path" Type="Str"></Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.2.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ActorList" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"E!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="ObjectManagerProxy_Activate" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="ObjectManagerProxy_WorkerActor" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"E!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_LDAQ_WDAlarm" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">High</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">RADRIS</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">LDAQ Watchdog Alarm</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Network,Alarming</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingCounter" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"E!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingDeltaT" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingInterval" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingIterations" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"E!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingMode" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"E!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_PollingTime" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_RVC300_WDAlarm" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">High</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">RADRIS</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">RVC300 Watchdog Alarm</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Network,Alarming</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_Set-PollingInterval" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_Set-PollingIterations" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"E!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_Set-PollingStartStop" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Watchdog_TPG366_WDAlarm" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">High</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">RADRIS</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">TPG300 Watchdog Alarm</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Network,Alarming</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="WatchdogProxy_Activate" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"E!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="WatchdogProxy_WorkerActor" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/RADRIS.lvproj/My Computer/SV.lib/RADRIS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"E!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
